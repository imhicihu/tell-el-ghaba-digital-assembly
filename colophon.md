* Software
    - [Duplicati](https://www.duplicati.com/) (open-source backup software)
    - [ImageOptim](https://github.com/ImageOptim/ImageOptim): image optimization
	- [Sqoosh](https://squoosh.app/): Image conpressor
    - [PDF.js](https://github.com/mozilla/pdf.js): PDF file renderer
    - [Jump](https://jump.moapp.software/): create internal jumps inside pdf documents
    - ![Tell el Gaba.jpg](https://bitbucket.org/repo/MAkj9k/images/3259614179-Tell%20el%20Gaba.jpg)
    - [ScriptUI Dialog Builder](https://scriptui.joonas.me/)

* Documentation
    - [JavaScript Tools Guide CC](https://estk.aenhancers.com/index.html)